# `ca.ntro.modules`


## JSweet et les modules

* Le `ntro-jsweet-plugin` va inclure les sources des dépendances avec un ajout à `sourceSets`
    * les `exports` du `module-info.java` sont ignorés (tout est exporté)


## Utiliser un autre JDK

Pour *JSweet*, il faut le JDK11

* JDK18 donne une erreur
* JDK17 OK? (à tester!)

*Ntro* avec le JDK18 et modulaires

```bash
export JAVA_HOME=~/.local/jdk-18.0.2.1/
export PATH=$JAVA_HOME/bin:$PATH
```

## TODO

1. Rebâtir les projets avec `module-info.java` (mais sinon ne pas toucher le code)
1. Choisir 1-2 projets démo
1. Plus tard: reconception, p.ex. pour le paquet `graphs`
