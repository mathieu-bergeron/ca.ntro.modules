# Reconception pour utiliser `module-info.java`

* Nouvelle limitation: le projet `ntro_jdk` ne peut pas ajouter des classes à un paquet du projet `ntro_core`

## Qu'est-ce qu'on a dans Ntro

* Des services (même chose que la couche système?):
    * reflection
    * factory
    * ...
* Des valeurs «du programme»:
    * `Stream`
    * `Path`
    * `Name` (identifiant)
    * `Tick`
    * `Future`
    * `Optionnal`
    * ...
* Les stream (c'est une valeur, mais on met dans un paquet séparé?)
    * `Mapper`
    * `Matcher`
    * ...
* Les graphes
* Des valeurs «d'un graphe d'objets» (manipuler des données):
    * `SimpleValue`
    * `Array`, `List`, `Map`
    * `UserDefinedObject`
    * ...
* Une couche système (accédée par les services):
    * `Thread`
    * `File`
    * `LocalTextFile`
    * `LocalStorage`
    * ...


## Roadmap

* Reconception juste ce qu'il faut pour:
    * Exemples de graphes d'objets en NtroJdk (sans NtroApp)
    * NtroApp et les app de 4F5
