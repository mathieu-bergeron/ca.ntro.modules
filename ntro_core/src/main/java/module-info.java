module ca.ntro.core {
	
	// JSWeet: exports are ignored. All sources are included via a gradle sourceSets directive
	
	                           // XXX: exporting only to ca.ntro.jdk
	exports ca.ntro.core.clock to ca.ntro.jdk;
	exports ca.ntro.core.data_structures;
	exports ca.ntro.core.edit_distance;
	exports ca.ntro.core.exceptions;
	exports ca.ntro.core.files;
	exports ca.ntro.core.graphs;

	// XXX: every package must be explicitely exported??
	// XXX: in the importing project, cannot add a class to
	//      an imported package
	exports ca.ntro.core.graphs.hierarchical_graph to ca.ntro.jdk;

	exports ca.ntro.core.graph_writer;
	exports ca.ntro.core.identifyers;
	exports ca.ntro.core.initialization;
	exports ca.ntro.core.json;
	exports ca.ntro.core.notifyers;
	exports ca.ntro.core.path;
	exports ca.ntro.core.reflection;
	
	
	// XXX: interfaces are exported for all
	exports ca.ntro.core.services;
	
	// XXX: implementations are exported only for _jdk/_fx/_jsweet 
	//      i.e. only where it will be needed in order to extend it 
	//      and create the Jdk/Fx/JSweet versions
	exports ca.ntro.core.services.implementations to ca.ntro.jdk, ca.ntro.jsweet, ca.ntro.fx;

	exports ca.ntro.core.storage;
	exports ca.ntro.core.stream;
	exports ca.ntro.core.task_graphs;
	exports ca.ntro.core.util;
	exports ca.ntro.core.validation;
	exports ca.ntro.core.values;
	exports ca.ntro.core.wrappers;
	exports ca.ntro.core.wrappers.future;
	

	
}
