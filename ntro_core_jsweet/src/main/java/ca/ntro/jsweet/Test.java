package ca.ntro.jsweet;

import ca.ntro.Ntro;

public class Test {
    
    public static void main(String[] args) {
        
        Object[] liste = new Object[] {"adf", 'a' ,null, 132};
        
        // XXX: on accède ici à la version JDK 
        //      sans avoir à initialiser
        Ntro.reflection();

        System.out.println(Ntro.toJson(liste));

    }

}
